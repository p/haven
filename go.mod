module janouch.name/haven

go 1.17

require (
	github.com/BurntSushi/xgb v0.0.0-20160522181843-27f122750802
	github.com/bytesparadise/libasciidoc v0.7.1-0.20221008082129-967103fe8df6
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	golang.org/x/image v0.0.0-20190802002840-cff245a6509b
)

require github.com/sirupsen/logrus v1.8.1 // indirect

require (
	github.com/alecthomas/chroma/v2 v2.3.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dlclark/regexp2 v1.4.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.9 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
