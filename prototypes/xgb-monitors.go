package main

import (
	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/xproto"
	"log"

	// Needs a patched local version with xcb-proto 1.12 and this fix:
	// -size := xgb.Pad((8 + (24 + xgb.Pad((int(NOutput) * 4)))))
	// +size := xgb.Pad((8 + (24 + xgb.Pad((int(Monitorinfo.NOutput) * 4)))))
	"github.com/BurntSushi/xgb/randr"
)

func main() {
	X, err := xgb.NewConn()
	if err != nil {
		log.Fatalln(err)
	}

	if err := randr.Init(X); err != nil {
		log.Fatalln(err)
	}

	setup := xproto.Setup(X)
	screen := setup.DefaultScreen(X)

	ms, err := randr.GetMonitors(X, screen.Root, true /* GetActive */).Reply()
	if err != nil {
		log.Fatalln(err)
	}

	for _, m := range ms.Monitors {
		reply, err := xproto.GetAtomName(X, m.Name).Reply()
		if err != nil {
			log.Fatalln(err)
		}

		log.Printf("Monitor %s %+v\n", reply.Name, m)
	}
}
